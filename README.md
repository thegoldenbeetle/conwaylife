## Game Life

## О решении

### Необходимые пакеты

jupyter notebook

Windows:
- python >= 3.7.5
- numpy >= 1.17.3
- scipy >= 1.4.1

Ubuntu:
- python >= 3.6.9
- numpy >= 1.18.2
- scipy >= 1.4.1
- python3-tk (При получении ошибки "No module named tkinter")

### Инструкция по запуску решения
*Открыть файл life.ipynb и выполнить там все ячейки*

### Примеры 

Глайдер:

![Launch](Gifs/glider.gif "Глайдер")

Ружье Госпера:

![Launch](Gifs/glider_gun.gif "Ружье Госпера")

Просто пример:

![Launch](Gifs/smth.gif "Просто пример")


